export default class SurveyEndpoint {
  constructor({url}) {
    this.url = url;
  }

  async getSurveyModel() {
    return await this.fetch(this.url, 'GET');
  }

  async addNewSurvey(data) {
    return await this.fetch(this.url, 'PUT', data);
  }

  async fetch(url, method, data) {
    const opts = {
      method: method,
      headers: new Headers({
        'Content-Type': 'application/json'
      })
    };

    if (data) {
      opts.body = JSON.stringify(data);
    }

    const response = await fetch(url, opts);
    return await response.json();
  }
}
